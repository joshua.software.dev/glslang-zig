#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

declare -a targets=(
    # linux
    "x86_64-linux-gnu"
    "x86_64-linux-gnu.2.17"
    "x86_64-linux-musl"
    "x86-linux-gnu"
    "x86-linux-gnu.2.17"
    "x86-linux-musl"
    "arm-linux-gnueabihf"
    "arm-linux-gnueabihf.2.17"
    "arm-linux-musleabihf"
    "aarch64-linux-gnu"
    "aarch64-linux-gnu.2.17"
    "aarch64-linux-musl"
    # https://github.com/ziglang/zig/issues/3340
    # "riscv64-linux-gnu"
    # "riscv64-linux-gnu.2.27"
    "riscv64-linux-musl"

    # windows
    "x86_64-windows-gnu"
    "x86-windows-gnu"
    "aarch64-windows-gnu"

    # macos
    "x86_64-macos-none"
    "aarch64-macos-none"
)

rm -rf include/
zig build generate_headers

for t in "${targets[@]}"
do
    printf "$t\n"
    zig build -D"target=$t" -D"optimize=ReleaseFast"
done
