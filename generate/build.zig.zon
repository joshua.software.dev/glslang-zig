.{
    .name = "generate",
    // This is a [Semantic Version](https://semver.org/).
    // In a future version of Zig it will be used for package deduplication.
    .version = "14.3.0",

    // This field is optional.
    // This is currently advisory only; Zig does not yet do anything
    // with this value.
    .minimum_zig_version = "0.12.0",

    // This field is optional.
    // Each dependency must either provide a `url` and `hash`, or a `path`.
    // `zig build --fetch` can be used to fetch all dependencies of a package, recursively.
    // Once all dependencies are fetched, `zig build` no longer requires
    // internet connectivity.
    .dependencies = .{
        // See `zig fetch --save <url>` for a command-line interface for adding dependencies.
        //.example = .{
        //    // When updating this field to a new URL, be sure to delete the corresponding
        //    // `hash`, otherwise you are communicating that you expect to find the old hash at
        //    // the new URL.
        //    .url = "https://example.com/foo.tar.gz",
        //
        //    // This is computed from the file contents of the directory of files that is
        //    // obtained after fetching `url` and applying the inclusion rules given by
        //    // `paths`.
        //    //
        //    // This field is the source of truth; packages do not come from a `url`; they
        //    // come from a `hash`. `url` is just one of many possible mirrors for how to
        //    // obtain a package matching this `hash`.
        //    //
        //    // Uses the [multihash](https://multiformats.io/multihash/) format.
        //    .hash = "...",
        //
        //    // When this is provided, the package is found in a directory relative to the
        //    // build root. In this case the package's hash is irrelevant and therefore not
        //    // computed. This field and `url` are mutually exclusive.
        //    .path = "foo",
        //},
        .glslang_src = .{
            .hash = "122096707a369b38cc66be5a6bc5083fd8f51fa640e65ffaa7f9a33f03450eaf8164",
            .url = "git+https://github.com/KhronosGroup/glslang.git#fa9c3deb49e035a8abcabe366f26aac010f6cbfb",
        },
        .python_w2c2zig = .{
            .hash = "1220a36c1d1024be4352f892fc6089550346f00f2741e92fb29fcce20dd08ff8ce53",
            .lazy = true,
            .url = "git+https://codeberg.org/joshua-software-dev/python_w2c2zig.git#018ec9b1f4b628974e7fa2fff9643af526ca4211",
        },
        .spirv_headers = .{
            .hash = "1220eff0275d47f8da9e66fe0f30cd0d9084bda079861a67c6a77528fbe82ed200bc",
            .url = "git+https://github.com/KhronosGroup/SPIRV-Headers.git#2acb319af38d43be3ea76bfabf3998e5281d8d12",
        },
        .spirv_tools = .{
            .hash = "1220244510c4e41974b60f1db782049add987edffb084634654592e3e818ef7623ad",
            .url = "git+https://github.com/KhronosGroup/SPIRV-Tools.git#0cfe9e7219148716dfd30b37f4d21753f098707a",
        },
    },

    // Specifies the set of files and directories that are included in this package.
    // Only files and directories listed here are included in the `hash` that
    // is computed for this package.
    // Paths are relative to the build root. Use the empty string (`""`) to refer to
    // the build root itself.
    // A directory listed here means that all files within, recursively, are included.
    .paths = .{
        // This makes *all* files, recursively, included in this package. It is generally
        // better to explicitly list the files and directories instead, to insure that
        // fetching from tarballs, file system paths, and version control all result
        // in the same contents hash.
        "",
        // For example...
        //"build.zig",
        //"build.zig.zon",
        //"src",
        //"LICENSE",
        //"README.md",
    },
}
