const builtin = @import("builtin");
const std = @import("std");


const PrepareSourceFilesStep = struct {
    step: std.Build.Step,
    glslang_src_path: []const u8,
    spirv_header_src_path: []const u8,
    spirv_tools_src_path: []const u8,

    pub fn create(
        b: *std.Build,
        glslang_src_path: []const u8,
        spirv_header_src_path: []const u8,
        spirv_tools_src_path: []const u8,
    ) !*@This() {
        const self = try b.allocator.create(@This());
        self.* = .{
            .step = std.Build.Step.init(.{
                .id = .custom,
                .name = "prepare glslang sources",
                .owner = b,
                .makeFn = make,
            }),
            .glslang_src_path = glslang_src_path,
            .spirv_header_src_path = spirv_header_src_path,
            .spirv_tools_src_path = spirv_tools_src_path,
        };
        return self;
    }

    fn file_not_found(b: *std.Build, from: []const u8, to: []const u8) !void {
        switch (b.host.result.os.tag) {
            // windows has poor support for symlinks, just copy the
            // files instead
            .windows => {
                // TODO: Reimplement this in zig
                const result = try std.process.Child.run(.{
                    .allocator = b.allocator,
                    .argv = &.{
                        "robocopy",
                        from,
                        to,
                        // recursive
                        "/e",
                        // quieter
                        "/nc",
                        "/ndl",
                        "/njh",
                        "/njs",
                        "/ns",
                    },
                    .max_output_bytes = std.math.maxInt(usize),
                });
                defer {
                    b.allocator.free(result.stderr);
                    b.allocator.free(result.stdout);
                }

                switch (result.term) {
                    .Exited => |e| switch (e) {
                        1 => {},
                        else => @panic("Unexpected return code when copying source files"),
                    },
                    else => @panic("Unexpected signal when copying source files")
                }
            },
            // only create symlink if it does not exist
            else => try std.fs.symLinkAbsolute(
                from,
                to,
                .{ .is_directory = true },
            ),
        }
    }

    fn make(step: *std.Build.Step, _: std.Progress.Node) !void {
        const b = step.owner;
        const self: *@This() = @fieldParentPtr("step", step);

        // ensure glslang's external dependencies are symlinked where it
        // expects for a cmake build
        const glslang_symlink_path = b.pathJoin(&.{ self.glslang_src_path, "External", "spirv-tools" });
        var dir1: ?std.fs.Dir = std.fs.openDirAbsolute(glslang_symlink_path, .{})
            catch |err| switch (err) {
                error.FileNotFound => blk: {
                    try file_not_found(b, self.spirv_tools_src_path, glslang_symlink_path);
                    break :blk null;
                },
                error.NotDir => blk: {
                    try std.fs.deleteFileAbsolute(glslang_symlink_path);
                    try file_not_found(b, self.spirv_tools_src_path, glslang_symlink_path);
                    break :blk null;
                },
                else => return err,
            };
        if (dir1) |*d| d.close();

        // ensure spirv-tools's external dependencies are symlinked where
        // glslang expects for a cmake build
        const spirv_tools_symlink_path = b.pathJoin(&.{ glslang_symlink_path, "external", "spirv-headers" });
        var dir2: ?std.fs.Dir = std.fs.openDirAbsolute(spirv_tools_symlink_path, .{})
            catch |err| switch (err) {
                error.FileNotFound => blk: {
                    try file_not_found(b, self.spirv_header_src_path, spirv_tools_symlink_path);
                    break :blk null;
                },
                error.NotDir => blk: {
                    try std.fs.deleteFileAbsolute(spirv_tools_symlink_path);
                    try file_not_found(b, self.spirv_header_src_path, spirv_tools_symlink_path);
                    break :blk null;
                },
                else => return err,
            };
        if (dir2) |*d| d.close();
    }
};

const CopyHeadersStep = struct {
    step: std.Build.Step,
    compile_dir: std.Build.LazyPath,
    output_dir: std.Build.LazyPath,

    pub fn create(b: *std.Build, compile_dir: std.Build.LazyPath, output_dir: std.Build.LazyPath) !*@This() {
        const self = try b.allocator.create(@This());
        self.* = .{
            .step = std.Build.Step.init(.{
                .id = .custom,
                .name = "copy glslang headers",
                .owner = b,
                .makeFn = make,
            }),
            .compile_dir = compile_dir,
            .output_dir = output_dir,
        };
        return self;
    }

    fn make(step: *std.Build.Step, progress: std.Progress.Node) !void {
        const b = step.owner;
        const self: *@This() = @fieldParentPtr("step", step);
        progress.setEstimatedTotalItems(3);

        const compile_path = self.compile_dir.getPath(b);
        const output_path = self.output_dir.getPath(b);

        {
            std.fs.makeDirAbsolute(output_path)
                catch |err| switch (err) {
                    error.PathAlreadyExists => {},
                    else => return err,
                };
            var output_dir = try std.fs.openDirAbsolute(output_path, .{});
            defer output_dir.close();
            try output_dir.makePath("glslang");
            try output_dir.makePath("spirv-tools");
        }
        progress.setCompletedItems(1);

        {
            const out_glslang_include_path = try std.fs.path.join(b.allocator, &.{
                compile_path,
                "include",
                "glslang",
            });
            defer b.allocator.free(out_glslang_include_path);

            var out_glslang_include_dir = try std.fs.openDirAbsolute(out_glslang_include_path, .{ .iterate = true, });
            defer out_glslang_include_dir.close();
            var iter = out_glslang_include_dir.iterateAssumeFirstIteration();
            while (try iter.next()) |entry| {
                if (std.mem.endsWith(u8, entry.name, ".h")) {
                    const src_path = try std.fs.path.join(b.allocator, &.{
                        out_glslang_include_path,
                        entry.name,
                    });
                    defer b.allocator.free(src_path);

                    const dst_path = try std.fs.path.join(b.allocator, &.{
                        output_path,
                        "glslang",
                        entry.name,
                    });
                    defer b.allocator.free(dst_path);

                    _ = try std.fs.updateFileAbsolute(src_path, dst_path, .{});
                }
            }
        }
        progress.setCompletedItems(2);

        {
            const out_external_include_path = try std.fs.path.join(b.allocator, &.{
                compile_path,
                "External",
                "spirv-tools",
            });
            defer b.allocator.free(out_external_include_path);

            var out_external_include_dir = try std.fs.openDirAbsolute(out_external_include_path, .{ .iterate = true, });
            defer out_external_include_dir.close();
            var iter = out_external_include_dir.iterateAssumeFirstIteration();
            while (try iter.next()) |entry| {
                if (std.mem.endsWith(u8, entry.name, ".h") or std.mem.endsWith(u8, entry.name, ".inc")) {
                    const src_path = try std.fs.path.join(b.allocator, &.{
                        out_external_include_path,
                        entry.name,
                    });
                    defer b.allocator.free(src_path);

                    const dst_path = try std.fs.path.join(b.allocator, &.{
                        output_path,
                        "spirv-tools",
                        entry.name,
                    });
                    defer b.allocator.free(dst_path);

                    _ = try std.fs.updateFileAbsolute(src_path, dst_path, .{});
                }
            }
        }
        progress.setCompletedItems(3);
    }
};

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    _ = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    _ = b.standardOptimizeOption(.{});

    const glslang_src_dep = b.dependency("glslang_src", .{});
    const spirv_headers_dep = b.dependency("spirv_headers", .{});
    const spirv_tools_dep = b.dependency("spirv_tools", .{});

    // obviously cmake is necessary to do a cmake build
    const cmake_path = b.findProgram(&.{ "cmake" }, &.{})
        catch |err| switch (err) {
            error.FileNotFound => "cmake",
            else => return err,
        };

    // if ninja is available, prefer it as the make program
    const ninja_path: ?[]const u8 = b.findProgram(&.{ "ninja" }, &.{})
        catch |err| switch (err) {
            error.FileNotFound => null,
            else => return err,
        };

    // glslang depends on python to generate a small number of files during its
    // build, check if it exists in the global $PATH.
    const python_path: ?[]const u8 = blk: {
        const path = b.findProgram(&.{ "python", "python3" }, &.{})
            catch |err| switch (err) {
                error.FileNotFound => break :blk null,
                else => return err,
            };

        const result = try std.process.Child.run(.{
            .allocator = b.allocator,
            .argv = &.{
                path,
                "--version",
            },
            .max_output_bytes = 4096,
        });

        switch (result.term) {
            .Exited => |e| if (e != 0) break :blk null,
            else => unreachable,
        }
        break :blk path;
    };

    // Delay creating symlinks so that they aren't created at all unless the
    // generate step is actually triggered manually.
    const prepare_step = try PrepareSourceFilesStep.create(
        b,
        glslang_src_dep.path(".").getPath(b),
        spirv_headers_dep.path(".").getPath(b),
        spirv_tools_dep.path(".").getPath(b),
    );

    // generate temporary output directory
    const compile_path = b.makeTempPath();

    const cmake_configure_cmd = b.addSystemCommand(&.{
        cmake_path,
        "-DCMAKE_CROSSCOMPILING=true",
        "-DCMAKE_SYSTEM_NAME=Windows",
        "-DCMAKE_SIZEOF_VOID_P=8",
        "-DCMAKE_BUILD_TYPE=Release",
        "-DENABLE_EXCEPTIONS=OFF",
        "-DENABLE_GLSLANG_BINARIES=ON",
        "-DENABLE_SPVREMAPPER=OFF",
        "-DGLSLANG_TESTS=OFF",
        "-DENABLE_HLSL=ON",
        "-DENABLE_OPT=ON",
        "-DENABLE_PCH=OFF",
        "-DSPIRV_SKIP_EXECUTABLES=ON",
        "-DSPIRV_SKIP_TESTS=ON",
        "-DENABLE_SPIRV_TOOLS_INSTALL=OFF",
        "-DSPIRV_TOOLS_BUILD_STATIC=ON",
        "-DSPIRV_TOOLS_LIBRARY_TYPE=STATIC",
    });
    cmake_configure_cmd.step.dependOn(&prepare_step.step);

    // force use of zig's cross platform capable `ar` and `ranlib`, instead
    // of relying on auto discovered ones that may or may not be installed
    // on the system.
    switch (builtin.os.tag) {
        .windows => {
            const ar_path = b.pathJoin(&.{ compile_path, "zig-ar.cmd" });
            {
                const ar_file = try std.fs.createFileAbsolute(ar_path, .{});
                defer ar_file.close();
                try ar_file.writer().print("@echo off\r\n\"{s}\" ar %*\r\n", .{ b.graph.zig_exe });
            }

            const ranlib_path = b.pathJoin(&.{ compile_path, "zig-ranlib.cmd" });
            {
                const ranlib_file = try std.fs.createFileAbsolute(ranlib_path, .{});
                defer ranlib_file.close();
                try ranlib_file.writer().print("@echo off\r\n\"{s}\" ranlib %*\r\n", .{ b.graph.zig_exe });
            }

            cmake_configure_cmd.addArg(b.fmt("-DCMAKE_AR={s}", .{ ar_path }));
            cmake_configure_cmd.addArg(b.fmt("-DCMAKE_RANLIB={s}", .{ ranlib_path }));
        },
        else => {
            const ar_path = b.pathJoin(&.{ compile_path, "zig-ar.sh" });
            {
                const ar_file = try std.fs.createFileAbsolute(ar_path, .{ .mode = 0o755 });
                defer ar_file.close();
                try ar_file.writer().print("#!/bin/sh\n\"{s}\" ar \"$@\"\n", .{ b.graph.zig_exe });
            }

            const ranlib_path = b.pathJoin(&.{ compile_path, "zig-ranlib.sh" });
            {
                const ranlib_file = try std.fs.createFileAbsolute(ranlib_path, .{ .mode = 0o755 });
                defer ranlib_file.close();
                try ranlib_file.writer().print("#!/bin/sh\n\"{s}\" ranlib \"$@\"\n", .{ b.graph.zig_exe });
            }

            cmake_configure_cmd.addArg(b.fmt("-DCMAKE_AR={s}", .{ ar_path }));
            cmake_configure_cmd.addArg(b.fmt("-DCMAKE_RANLIB={s}", .{ ranlib_path }));
        },
    }

    if (python_path == null) {
        const python_w2c2zig_dep = b.lazyDependency("python_w2c2zig", .{ .target = b.host, .optimize = .ReleaseFast });
        if (python_w2c2zig_dep) |dep| {
            const python_dir_fixup = b.addWriteFiles();
            _ = python_dir_fixup.addCopyFileToSource(
                dep.artifact("CPython").getEmittedBin(),
                b.fmt("zig-cache/bin/python3{s}", .{ b.host.result.exeFileExt() }),
            );
            cmake_configure_cmd.step.dependOn(&python_dir_fixup.step);
            cmake_configure_cmd.addArg(b.fmt("-DPython3_ROOT_DIR='{s}'", .{ b.pathFromRoot("zig-cache/") }));
            cmake_configure_cmd.addArg(
                b.fmt(
                    "-DPython3_EXECUTABLE='{s}{s}'",
                    .{
                        b.pathFromRoot("zig-cache/bin/python3"),
                        b.host.result.exeFileExt(),
                    },
                ),
            );
        }
    }

    if (ninja_path != null) {
        cmake_configure_cmd.addArg("-G=Ninja");
    }

    cmake_configure_cmd.addArgs(&.{
        // output to temporary directory
        b.fmt("-B {s}", .{ compile_path }),
        // compile dependency directory we prepared earlier
        glslang_src_dep.path(".").getPath(b),
    });

    // zig cross compile is awesome :D
    cmake_configure_cmd.setEnvironmentVariable(
        "CC",
        b.fmt("'{s}' cc --target=x86_64-windows-gnu", .{ b.graph.zig_exe }),
    );
    cmake_configure_cmd.setEnvironmentVariable(
        "CC_FLAGS",
        b.fmt("-fuse-ld='\"{s}\" cc --target=x86_64-windows-gnu'", .{ b.graph.zig_exe }),
    );
    cmake_configure_cmd.setEnvironmentVariable(
        "CXX",
        b.fmt("'{s}' c++ --target=x86_64-windows-gnu", .{ b.graph.zig_exe }),
    );
    cmake_configure_cmd.setEnvironmentVariable(
        "CXX_FLAGS",
        b.fmt("-fuse-ld='\"{s}\" c++ --target=x86_64-windows-gnu'", .{ b.graph.zig_exe }),
    );

    // it might be possible to run only the commands necessary to generate the
    // header files we need without actually running a build, but I've no idea
    // how to make cmake do such a thing, if its even possible without modifying
    // the cmake build scripts.
    const cmake_build_cmd = b.addSystemCommand(&.{
        cmake_path,
        "--build",
        compile_path,
        "-j",
        // TODO: maybe make this respect zig's -j flag?
        b.fmt("{d}", .{ @max(1, try std.Thread.getCpuCount()) }),
    });
    cmake_build_cmd.step.dependOn(&cmake_configure_cmd.step);
    // spirv-tools generates a file with a timestamp in its build files. To
    // ensure reproducable builds, override it with unix epoch.
    cmake_build_cmd.setEnvironmentVariable("SOURCE_DATE_EPOCH", "0");

    // finally, after the build finishes, we need to copy the headers files
    // generated to the "include" folder of this repo so the zig build script
    // can use them. The current "generate_headers" function however, is run
    // ahead of the build, so we need to place the code that does the copying
    // into a custom std.Build.Step, so we can wait until after the build step
    // finishes for it to run.
    const copy_step = try CopyHeadersStep.create(
        b,
        // this is misleading, a LazyPath init with `.cwd_relative` takes an
        // absolute path and transforms it into a zig builder relative path
        .{ .cwd_relative = compile_path },
        b.path("../include/"),
    );
    copy_step.step.dependOn(&cmake_build_cmd.step);

    b.getInstallStep().dependOn(&copy_step.step);
}
