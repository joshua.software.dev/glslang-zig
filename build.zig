const builtin = @import("builtin");
const std = @import("std");

const spirv_tools_zigbuild = @import("spirv_tools_zigbuild");


const DEFINE = struct {
    name: []const u8,
    value: []const u8,
};

fn build_libglslang(
    b: *std.Build,
    glslang_src_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    args: anytype,
) *std.Build.Step.Compile {
    const libglslang = b.addStaticLibrary(.{
        .name = "glslang",
        .target = args.target,
        .optimize = args.optimize,
    });

    libglslang.root_module.pic = true;
    libglslang.linkLibC();
    libglslang.linkLibCpp();
    libglslang.addIncludePath(glslang_src_dep.path(""));

    for (defines) |define| {
        libglslang.defineCMacro(define.name, define.value);
    }

    libglslang.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/CInterface/glslang_c_interface.cpp"),
        .flags = flags,
    });

    return libglslang;
}

fn build_libglslang_drl(
    b: *std.Build,
    glslang_src_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    args: anytype,
) *std.Build.Step.Compile {
    const libglslang_drl = b.addStaticLibrary(.{
        .name = "glslang-default-resource-limits",
        .target = args.target,
        .optimize = args.optimize,
    });

    libglslang_drl.root_module.pic = true;
    libglslang_drl.linkLibC();
    libglslang_drl.linkLibCpp();
    libglslang_drl.addIncludePath(glslang_src_dep.path(""));

    for (defines) |define| {
        libglslang_drl.defineCMacro(define.name, define.value);
    }

    libglslang_drl.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/ResourceLimits/resource_limits_c.cpp"),
        .flags = flags,
    });
    libglslang_drl.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/ResourceLimits/ResourceLimits.cpp"),
        .flags = flags,
    });

    return libglslang_drl;
}

fn build_generic_code_gen(
    b: *std.Build,
    glslang_src_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    args: anytype,
) *std.Build.Step.Compile {
    const generic_code_gen = b.addStaticLibrary(.{
        .name = "GenericCodeGen",
        .target = args.target,
        .optimize = args.optimize,
    });

    generic_code_gen.root_module.pic = true;
    generic_code_gen.linkLibC();
    generic_code_gen.linkLibCpp();

    for (defines) |define| {
        generic_code_gen.defineCMacro(define.name, define.value);
    }

    generic_code_gen.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/GenericCodeGen/CodeGen.cpp"),
        .flags = flags,
    });
    generic_code_gen.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/GenericCodeGen/Link.cpp"),
        .flags = flags,
    });

    return generic_code_gen;
}

fn build_machine_independent(
    b: *std.Build,
    glslang_generated_headers: *std.Build.Step.WriteFile,
    glslang_src_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    args: anytype,
) !*std.Build.Step.Compile {
    const machine_independent = b.addStaticLibrary(.{
        .name = "MachineIndependent",
        .target = args.target,
        .optimize = args.optimize,
    });

    machine_independent.root_module.pic = true;
    machine_independent.linkLibC();
    machine_independent.linkLibCpp();
    machine_independent.addIncludePath(glslang_generated_headers.getDirectory());

    for (defines) |define| {
        machine_independent.defineCMacro(define.name, define.value);
    }

    const mi_files: []const []const u8 = &.{
        "glslang/HLSL/hlslAttributes.cpp",
        "glslang/HLSL/hlslGrammar.cpp",
        "glslang/HLSL/hlslOpMap.cpp",
        "glslang/HLSL/hlslParseables.cpp",
        "glslang/HLSL/hlslParseHelper.cpp",
        "glslang/HLSL/hlslScanContext.cpp",
        "glslang/HLSL/hlslTokenStream.cpp",
        "glslang/MachineIndependent/attribute.cpp",
        "glslang/MachineIndependent/Constant.cpp",
        "glslang/MachineIndependent/InfoSink.cpp",
        "glslang/MachineIndependent/Initialize.cpp",
        "glslang/MachineIndependent/Intermediate.cpp",
        "glslang/MachineIndependent/intermOut.cpp",
        "glslang/MachineIndependent/IntermTraverse.cpp",
        "glslang/MachineIndependent/iomapper.cpp",
        "glslang/MachineIndependent/limits.cpp",
        "glslang/MachineIndependent/linkValidate.cpp",
        "glslang/MachineIndependent/parseConst.cpp",
        "glslang/MachineIndependent/ParseContextBase.cpp",
        "glslang/MachineIndependent/ParseHelper.cpp",
        "glslang/MachineIndependent/PoolAlloc.cpp",
        "glslang/MachineIndependent/preprocessor/Pp.cpp",
        "glslang/MachineIndependent/preprocessor/PpAtom.cpp",
        "glslang/MachineIndependent/preprocessor/PpContext.cpp",
        "glslang/MachineIndependent/preprocessor/PpScanner.cpp",
        "glslang/MachineIndependent/preprocessor/PpTokens.cpp",
        "glslang/MachineIndependent/propagateNoContraction.cpp",
        "glslang/MachineIndependent/reflection.cpp",
        "glslang/MachineIndependent/RemoveTree.cpp",
        "glslang/MachineIndependent/Scan.cpp",
        "glslang/MachineIndependent/ShaderLang.cpp",
        "glslang/MachineIndependent/SpirvIntrinsics.cpp",
        "glslang/MachineIndependent/SymbolTable.cpp",
        "glslang/MachineIndependent/Versions.cpp",
    };

    for (mi_files) |file| {
        machine_independent.addCSourceFile(.{
            .file = glslang_src_dep.path(file),
            .flags = flags,
        });
    }

    var temp_flags = std.ArrayList([]const u8).init(b.allocator);
    defer temp_flags.deinit();
    try temp_flags.appendSlice(flags);
    try temp_flags.append("-Wno-unused-but-set-variable");
    machine_independent.addCSourceFile(.{
        .file = glslang_src_dep.path("glslang/MachineIndependent/glslang_tab.cpp"),
        .flags = temp_flags.items,
    });

    return machine_independent;
}

fn build_os_dependent(
    b: *std.Build,
    glslang_src_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    args: anytype,
) *std.Build.Step.Compile {
    const os_dependent = b.addStaticLibrary(.{
        .name = "OSDependent",
        .target = args.target,
        .optimize = args.optimize,
    });

    os_dependent.root_module.pic = true;
    os_dependent.linkLibC();
    os_dependent.linkLibCpp();

    for (defines) |define| {
        os_dependent.defineCMacro(define.name, define.value);
    }

    if (args.target.result.os.tag == .windows) {
        os_dependent.addCSourceFile(.{
            .file = glslang_src_dep.path("glslang/OSDependent/Windows/ossource.cpp"),
            .flags = flags,
        });
    } else {
        os_dependent.addCSourceFile(.{
            .file = glslang_src_dep.path("glslang/OSDependent/Unix/ossource.cpp"),
            .flags = flags,
        });
    }

    return os_dependent;
}

fn build_spirv(
    b: *std.Build,
    glslang_generated_headers: *std.Build.Step.WriteFile,
    glslang_src_dep: *std.Build.Dependency,
    spirv_tools_dep: *std.Build.Dependency,
    defines: []const DEFINE,
    flags: []const []const u8,
    spirv_tools: ?*std.Build.Step.Compile,
    args: anytype,
) *std.Build.Step.Compile {
    const spirv = b.addStaticLibrary(.{
        .name = "SPIRV",
        .target = args.target,
        .optimize = args.optimize,
    });

    spirv.root_module.pic = true;
    spirv.linkLibC();
    spirv.linkLibCpp();
    spirv.addIncludePath(glslang_generated_headers.getDirectory());
    spirv.addIncludePath(glslang_src_dep.path(""));

    if (spirv_tools) |lib| {
        spirv.linkLibrary(lib);
        spirv.addIncludePath(spirv_tools_dep.path("include/"));
    }

    for (defines) |define| {
        spirv.defineCMacro(define.name, define.value);
    }

    const spirv_files: []const []const u8 = &.{
        "SPIRV/GlslangToSpv.cpp",
        "SPIRV/InReadableOrder.cpp",
        "SPIRV/Logger.cpp",
        "SPIRV/SpvBuilder.cpp",
        "SPIRV/SpvPostProcess.cpp",
        "SPIRV/doc.cpp",
        "SPIRV/SpvTools.cpp",
        "SPIRV/disassemble.cpp",
        "SPIRV/CInterface/spirv_c_interface.cpp",
    };

    for (spirv_files) |file| {
        spirv.addCSourceFile(.{
            .file = glslang_src_dep.path(file),
            .flags = flags,
        });
    }

    return spirv;
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const enable_opt =
        if (b.option(
            bool,
            "enable_opt",
            "Enable the spirv-tools optimizer, must be enabled to generate valid SPIRV from HLSL, default=true",
        )) |opt|
            opt
        else
            true;

    const enable_ubsan =
        if (b.option(
            bool,
            "enable_ubsan",
            "Enable zig's (really llvm's) Undefined Behavior Sanitizer runtime, default=false",
        )) |opt|
            opt
        else
            false;

    const install_static_libs =
        if (b.option(
            bool,
            "install_static_libs",
            "Include static libraries in output, useful for compiling into projects outside glslang, default=true",
        )) |opt|
            opt
        else
            true;

    const warnings_as_errors =
        if (b.option(
            bool,
            "warnings_as_errors",
            "Enable to treat all compiler warnings into errors (-Werror), default=true",
        )) |opt|
            opt
        else
            true;

    const spirv_tools_git_hash_override: []const u8 = b.fmt(
        "{x}",
        .{ try spirv_tools_zigbuild.get_spirv_tools_git_hash(b, b.pathFromRoot("generate/build.zig.zon")) },
    );

    const generate_dep = b.dependency("generate", .{});
    const glslang_src_dep = generate_dep.builder.dependency("glslang_src", .{});
    const spirv_headers_dep = generate_dep.builder.dependency("spirv_headers", .{});
    const spirv_tools_dep = generate_dep.builder.dependency("spirv_tools", .{});
    const spirv_tools_zigbuild_dep = b.dependency("spirv_tools_zigbuild", .{
        .target = target,
        .optimize = optimize,
        .enable_opt = enable_opt,
        .enable_ubsan = enable_ubsan,
        .spirv_headers_path = spirv_headers_dep.path("").getPath(b),
        .spirv_tools_path = spirv_tools_dep.path("").getPath(b),
        .spirv_tools_git_hash_override = spirv_tools_git_hash_override,
        .warnings_as_errors = warnings_as_errors,
    });

    const generate_step = b.step(
        "generate",
        "Generate the glslang headers, only necessary when updating glslang version. " ++
        "Requires that cmake and python/python3 are available in $PATH",
    );
    generate_step.dependOn(generate_dep.builder.getInstallStep());

    const glslang_osinclude_name = switch (target.result.os.tag) {
        .windows => "GLSLANG_OSINCLUDE_WIN32",
        else => "GLSLANG_OSINCLUDE_UNIX",
    };

    const glslang_defines: []const DEFINE = &.{
        .{
            .name = glslang_osinclude_name,
            .value = "1",
        },
        .{ .name = "GLSLANG_SINGLE_THREADED", .value = if (target.result.os.tag == .wasi) "1" else "0" },
        .{
            .name = "ENABLE_HLSL",
            .value = "1",
        },
        .{
            .name = "ENABLE_OPT",
            .value = if (enable_opt) "1" else "0",
        },
        .{
            .name = "NDEBUG",
            .value = "1",
        },
    };

    var flags = blk: {
        var flag_array_list = std.ArrayList([]const u8).init(b.allocator);
        try flag_array_list.appendSlice(&.{
            "-std=c++17",
            "-Wall",
            "-Wuninitialized",
            "-Wunused",
            "-Wunused-local-typedefs",
            "-Wunused-parameter",
            "-Wunused-value",
            "-Wunused-variable",
            "-fno-rtti",
            "-fno-exceptions",
        });

        if (!enable_ubsan) {
            try flag_array_list.append("-fno-sanitize=undefined");
        }

        if (warnings_as_errors) {
            try flag_array_list.append("-Werror");
        }

        break :blk flag_array_list;
    };
    defer flags.deinit();

    const opt_flags = blk: {
        var flag_set = std.StringArrayHashMap(void).init(b.allocator);
        defer flag_set.deinit();

        for (flags.items) |flag| {
            try flag_set.put(flag, {});
        }

        const temp_flags: []const []const u8 = &.{
            "-Wextra-semi",
            "-Wextra",
            "-Wnon-virtual-dtor",
            "-Wno-missing-field-initializers",
            "-Wno-self-assign",
            "-Wno-long-long",
            "-Wshadow",
            "-Wundef",
            "-Wconversion",
            "-Wno-sign-conversion",
            "-ftemplate-depth=1024",
        };

        for (temp_flags) |flag| {
            try flag_set.put(flag, {});
        }

        var flag_array_list = std.ArrayList([]const u8).init(b.allocator);
        try flag_array_list.appendSlice(flag_set.keys());

        break :blk flag_array_list;
    };
    defer opt_flags.deinit();

    const glslang_generated_headers = b.addWriteFiles();
    {
        const generate_build_info = b.addExecutable(.{
            .name = "generate_build_info",
            .root_source_file = b.path("src/generate_build_info.zig"),
            .target = b.host,
            .optimize = .Debug,
        });
        const run_gen_build_info = b.addRunArtifact(generate_build_info);
        run_gen_build_info.addDirectoryArg(glslang_src_dep.path(""));
        const @"build_info.h" = run_gen_build_info.addOutputFileArg("build_info.h");
        _ = glslang_generated_headers.addCopyFile(@"build_info.h", "glslang/build_info.h");
    }

    {
        const generate_extension_headers = b.addExecutable(.{
            .name = "generate_extension_headers",
            .root_source_file = b.path("src/generate_extension_headers.zig"),
            .target = b.host,
            .optimize = .Debug,
        });
        const run_gen_ext_headers = b.addRunArtifact(generate_extension_headers);
        run_gen_ext_headers.addDirectoryArg(glslang_src_dep.path("glslang/ExtensionHeaders"));
        const @"glsl_intrinsic_header.h" = run_gen_ext_headers.addOutputFileArg("glsl_intrinsic_header.h");
        _ = glslang_generated_headers.addCopyFile(@"glsl_intrinsic_header.h", "glslang/glsl_intrinsic_header.h");
    }

    const args = .{
        .target = target,
        .optimize = optimize,
    };
    const libglslang = build_libglslang(b, glslang_src_dep, glslang_defines, flags.items, args);
    const libglslang_drl = build_libglslang_drl(b, glslang_src_dep, glslang_defines, flags.items, args);
    const generic_code_gen = build_generic_code_gen(b, glslang_src_dep, glslang_defines, flags.items, args);
    const machine_independent = try build_machine_independent(
        b,
        glslang_generated_headers,
        glslang_src_dep,
        glslang_defines,
        flags.items,
        args,
    );
    const os_dependent = build_os_dependent(b, glslang_src_dep, glslang_defines, flags.items, args);
    const spirv = build_spirv(
        b,
        glslang_generated_headers,
        glslang_src_dep,
        spirv_tools_dep,
        glslang_defines,
        flags.items,
        if (enable_opt)
            spirv_tools_zigbuild_dep.artifact("SPIRV-Tools")
        else
            null,
        args,
    );

    const exe = b.addExecutable(.{
        .name = "glslangValidator",
        .target = target,
        .optimize = optimize,
    });

    exe.linkLibC();
    exe.linkLibCpp();
    exe.linkLibrary(libglslang);
    exe.linkLibrary(libglslang_drl);
    exe.linkLibrary(generic_code_gen);
    exe.linkLibrary(machine_independent);
    exe.linkLibrary(os_dependent);
    exe.linkLibrary(spirv);

    exe.addIncludePath(glslang_generated_headers.getDirectory());
    exe.addIncludePath(glslang_src_dep.path(""));
    if (enable_opt) exe.addIncludePath(spirv_tools_dep.path("include/"));

    for (glslang_defines) |define| {
        exe.defineCMacro(define.name, define.value);
    }

    try flags.append("-Wshorten-64-to-32");
    exe.addCSourceFile(.{
        .file = glslang_src_dep.path("StandAlone/StandAlone.cpp"),
        .flags = flags.items,
    });

    if (optimize != .Debug) {
        exe.root_module.strip = true;
        exe.want_lto =
            target.result.os.tag != .macos and // unsupported
            target.result.os.tag != .windows and // llvm linker bug workaround
            !(target.result.cpu.arch == .riscv64 and target.result.abi.isGnu()); // riscv64 teething issues
    }

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(exe);

    if (install_static_libs) {
        b.installArtifact(libglslang);
        b.installArtifact(libglslang_drl);
        b.installArtifact(generic_code_gen);
        b.installArtifact(machine_independent);
        b.installArtifact(os_dependent);
        b.installArtifact(spirv);
    }
}
