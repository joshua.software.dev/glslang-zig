# glslang-zig

This is a packaging of glslangValidator (sometimes just called [glslang](https://github.com/KhronosGroup/glslang)) for the zig build system.

# Usage

In your `build.zig`:
```zig
const ShaderCompiler = struct {
    compiler_kind: enum { glslang, glslc },
    run_step: *std.Build.Step.Run,
};

/// This function makes it easy to prefer using an install of glslang or glslc
/// already installed on the system if it is available.
///
/// Note, that the run step returned has no arguments set, and they will need
/// to be given to the step afterwards. Set `use_fallback` to false to prevent
/// compiling glslangValidator from source if it not found in $PATH.
fn get_shader_compiler(b: *std.Build, use_fallback: bool) !ShaderCompiler {
    const maybe_glslang_path = b.findProgram(&.{ "glslang", "glslangValidator" }, &.{})
        catch |err| switch (err) {
            error.FileNotFound => null,
            else => return err,
        };
    if (maybe_glslang_path) |glslang_path| {
        return .{
            .compiler_kind = .glslang,
            .run_step = b.addSystemCommand(&.{ glslang_path }),
        };
    }

    const maybe_glslc_path = b.findProgram(&.{ "glslc" }, &.{})
        catch |err| switch (err) {
            error.FileNotFound => null,
            else => return err,
        };
    if (maybe_glslc_path) |glslc_path| {
        return .{
            .compiler_kind = .glslc,
            .run_step = b.addSystemCommand(&.{ glslc_path }),
        };
    }

    if (use_fallback) {
        const glslang_dep = b.lazyDependency("glslang", .{
            // if you want to use glslang at build time, you likely want a native
            // binary rather than a binary for your current target
            .target = b.host,
            // glslang uses -O3, so recommending ReleaseFast to match that
            .optimize = .ReleaseFast,
        });

        if (glslang_dep) |dep| {
            return .{
                .compiler_kind = .glslang,
                .run_step = b.addRunArtifact(dep.artifact("glslangValidator")),
            };
        }
    }

    return error.NoShaderCompilerFound;
}

pub fn build(b: *std.Build) !void {
    // ...

    const compile_frag_step = try get_shader_compiler(b, true);
    switch (compile_frag_step.compiler_kind) {
        .glslang => compile_frag_step.run_step.addArg("-V"),
        .glslc => {
            compile_frag_step.run_step.addArg("-O");
            compile_frag_step.run_step.addArg("-fshader-stage=frag");
        },
    }
    compile_frag_step.run_step.addFileArg(b.path("path/to/input/shader/shader.frag.glsl"));
    compile_frag_step.run_step.addArg("-o");
    const frag_spv = compile_frag_step.run_step.addOutputFileArg("shader.frag.spv");
    my_exe.root_module.addAnonymousImport(
        // this can be named anything you want as long as you don't add two imports
        // with the same name, and there is no file with the same name in the
        // directory of the source file you import it from. Its named the same as
        // the output file here for convenience, not because it is required.
        "shader.frag.spv",
        .{ .root_source_file = frag_spv },
    );

    // ...
}
```

in the file you wish to use the shader (for example, it could be `src/example.zig` in this case):
```zig
// make sure there is no file named "shader.frag.spv" in "src/" or you might
// have troubles with this.
const frag_shader: []const u8 = @embedFile("shader.frag.spv");

fn do_the_thing() void {
    std.debug.print("Shader Size in bytes: {d}\n", .{ frag_shader.len });
}
```

since many Vulkan API's want an array of u32 instead of an array of bytes, it can be helpful to translate the @embedFile at compile time:
```zig
const builtin = @import("builtin");
const vk = @import("vk");

// you likely need more functions than this loaded in a real application
const VkDeviceWrapperType = vk.DeviceWrapper(&.{.{ device_commands = .{ .createShaderModule = true }}});
fn do_the_thing(device: vk.Device, device_wrapper: VkDeviceWrapperType) !void
{
    // ...

    // While this array literal example below is dramatically trimmed for
    // brevity, it serves as a demonstration of what the more complex block
    // further down compiles down to. The compiler has more do more work to get
    // there, but these two ways of defining your shader sources as u32 are
    // identical at runtime.
    pub const manual_frag_src: []const u32 = &.{ 0x00FF, 0x10 };

    // Shader modules are loaded from u32 arrays, and `@alignOf(u32) == 4`.
    // Unfortunately, @embedFile is an `[] align (1) const u8`, and compounding
    // problems, @alignCast cannot increase the pointer alignment of a type to
    // fix this. However, @alignCast does assert that it was able to do the
    // conversion because it was already aligned correctly. By making this a
    // comptime block, we are able to take advantage of this and verify at
    // compile time that the embedded shader is able to be coerced safely into
    // an `[] align (4) const u8`. Then, we can safely use
    // `std.mem.bytesAsSlice()` to cast these well aligned bytes into a
    // `[] align (4) const u32`, aka a `[]const u32` for short.
    const frag_src: []const u32 = comptime blk: {
        const raw: []const u8 = @embedFile("imgui.frag.spv");
        const aligned: [] align(@alignOf(u32)) const u8 = @alignCast(raw);
        break :blk std.mem.bytesAsSlice(u32, aligned[0..]);
    };

    const frag_module = try device_wrapper.createShaderModule(
        device,
        &.{
            .code_size = frag_src.len * @sizeOf(u32),
            .p_code = frag_src.ptr,
        },
        null,
    );

    // ...
}
```

## Master

This is the master branch, it targets the latest zig stable version. At the time of writing, that means it targets compatibility with zig 0.13.0. Later may possibly work, but zig is an evolving language, so it also may not still be compatible.
