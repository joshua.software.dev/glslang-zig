const std = @import("std");


const ParserState = enum {
    @"first_pre_major_#",
    @"second_pre_major_#",
    pre_major_space,
    major_version,
    pre_minor_dot,
    minor_version,
    pre_patch_dot,
    patch_version,
    optional_flavor_postfix,
    pre_year_whitespace,
    year_digits,
    pre_month_dash,
    month_digit_1,
    month_digit_2,
    pre_day_dash,
    day_digit_1,
    day_digit_2,
};

fn deduce_software_version(allocator: std.mem.Allocator, changes_file: *std.fs.File) ![]const u8 {
    const file_buffer = blk: {
        defer changes_file.close();

        var file_buffer = std.ArrayList(u8).init(allocator);
        try changes_file.reader().readAllArrayList(&file_buffer, std.math.maxInt(usize));
        break :blk file_buffer;
    };
    defer file_buffer.deinit();

    var splitter = std.mem.splitScalar(u8, file_buffer.items, '\n');
    while (splitter.next()) |split_item| {
        const trimmed = std.mem.trim(u8, split_item, &std.ascii.whitespace);
        if (trimmed.len == 0) continue;

        var parser_state: ParserState = .@"first_pre_major_#";
        var i: usize = 0;
        var version_end_pos: usize = 0;
        while (i <= trimmed.len) : (i += 1) {
            switch (parser_state) {
                .@"first_pre_major_#" => {
                    if (trimmed[i] == '#') {
                        parser_state = .@"second_pre_major_#";
                        continue;
                    }
                    break;
                },
                .@"second_pre_major_#" => {
                    if (trimmed[i] == '#') {
                        parser_state = .pre_major_space;
                        continue;
                    }
                    break;
                },
                .pre_major_space => {
                    if (trimmed[i] == ' ') {
                        parser_state = .major_version;
                        continue;
                    }
                    break;
                },
                .major_version => switch (trimmed[i]) {
                    '0'...'9' => continue,
                    '.' => {
                        parser_state = .pre_minor_dot;
                        continue;
                    },
                    else => break,
                },
                .pre_minor_dot => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .minor_version;
                        continue;
                    },
                    else => break,
                },
                .minor_version => switch (trimmed[i]) {
                    '0'...'9' => continue,
                    '.' => {
                        parser_state = .pre_patch_dot;
                        continue;
                    },
                    else => break,
                },
                .pre_patch_dot => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .patch_version;
                        continue;
                    },
                    else => break,
                },
                .patch_version => switch (trimmed[i]) {
                    '0'...'9' => continue,
                    '-' => {
                        parser_state = .optional_flavor_postfix;
                        continue;
                    },
                    ' ' => {
                        version_end_pos = i;
                        parser_state = .pre_year_whitespace;
                        continue;
                    },
                    else => break,
                },
                .optional_flavor_postfix => {
                    if (std.ascii.isWhitespace(trimmed[i])) {
                        version_end_pos = i;
                        parser_state = .pre_year_whitespace;
                        continue;
                    }

                    continue;
                },
                .pre_year_whitespace => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .year_digits;
                        continue;
                    },
                    else => break,
                },
                .year_digits => switch (trimmed[i]) {
                    '0'...'9' => continue,
                    '-' => {
                        parser_state = .pre_month_dash;
                        continue;
                    },
                    else => break,
                },
                .pre_month_dash => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .month_digit_1;
                        continue;
                    },
                    else => break,
                },
                .month_digit_1 => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .month_digit_2;
                        continue;
                    },
                    else => break,
                },
                .month_digit_2 => switch (trimmed[i]) {
                    '-' => {
                        parser_state = .pre_day_dash;
                        continue;
                    },
                    else => break,
                },
                .pre_day_dash => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .day_digit_1;
                        continue;
                    },
                    else => break,
                },
                .day_digit_1 => switch (trimmed[i]) {
                    '0'...'9' => {
                        parser_state = .day_digit_2;
                        continue;
                    },
                    else => break,
                },
                .day_digit_2 => {
                    if (trimmed.len > i) {
                        if (std.mem.trimLeft(u8, trimmed[i..], &std.ascii.whitespace).len > 0) {
                            break;
                        }
                    }

                    return allocator.dupe(u8, trimmed[3..version_end_pos]);
                },
            }
        }
    }

    return error.DeducingVersionFailed;
}

fn replace(buffer: *std.ArrayList(u8), search_for: []const u8, replace_with: []const u8) !void {
    if (std.mem.indexOf(u8, buffer.items, search_for)) |pos| {
        try buffer.replaceRange(pos, search_for.len, replace_with);
        return;
    }

    return error.ReplacementFailed;
}

fn write_buffer_to_file(cwd: std.fs.Dir, output_file_path: []const u8, buffer: []const u8) !void {
    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(buffer);
    try output_file.setEndPos(buffer.len);
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);
    if (args.len != 3) {
        std.debug.print("usage: {s} <glslang-repo-path> <output-file-path>\n", .{ args[0] });
        return 1;
    }

    const glslang_repo_path = args[1];
    const output_file_path = args[2];

    const cwd = std.fs.cwd();

    var buffer = std.ArrayList(u8).init(allocator);
    defer buffer.deinit();
    const version_string = blk: {
        var repo_dir = try cwd.openDir(glslang_repo_path, .{});
        defer repo_dir.close();

        var template_file = try repo_dir.openFile("build_info.h.tmpl", .{});
        defer template_file.close();
        try template_file.reader().readAllArrayList(&buffer, std.math.maxInt(usize));

        var changes_file = try repo_dir.openFile("CHANGES.md", .{});
        // `deduce_software_version` must close the file
        break :blk try deduce_software_version(allocator, &changes_file);
    };
    defer allocator.free(version_string);

    {
        var ver_arena = std.heap.ArenaAllocator.init(allocator);
        defer ver_arena.deinit();
        const sem_ver = try std.SemanticVersion.parse(version_string);
        const major_replacement = try std.fmt.allocPrint(ver_arena.allocator(), "{d}", .{ sem_ver.major });
        const minor_replacement = try std.fmt.allocPrint(ver_arena.allocator(), "{d}", .{ sem_ver.minor });
        const patch_replacement = try std.fmt.allocPrint(ver_arena.allocator(), "{d}", .{ sem_ver.patch });
        try replace(&buffer, "@major@", major_replacement);
        try replace(&buffer, "@minor@", minor_replacement);
        try replace(&buffer, "@patch@", patch_replacement);
        try replace(&buffer, "@flavor@", sem_ver.pre orelse "");
    }

    try write_buffer_to_file(cwd, output_file_path, buffer.items);

    return 0;
}
