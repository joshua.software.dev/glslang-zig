const std = @import("std");


fn write_buffer_to_file(cwd: std.fs.Dir, output_file_path: []const u8, buffer: []const u8) !void {
    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(buffer);
    try output_file.setEndPos(buffer.len);
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);
    if (args.len != 3) {
        std.debug.print("usage: {s} <input-dir-path> <output-file-path>\n", .{ args[0] });
        return 1;
    }

    const input_dir_path = args[1];
    const output_file_path = args[2];

    const cwd = std.fs.cwd();
    var input_dir = try cwd.openDir(input_dir_path, .{ .iterate = true });
    defer input_dir.close();

    var output_buffer = std.ArrayList(u8).init(allocator);
    defer output_buffer.deinit();

    try output_buffer.appendSlice(
        \\/***************************************************************************
        \\ *
        \\ * Copyright (c) 2015-2021 The Khronos Group Inc.
        \\ * Copyright (c) 2015-2021 Valve Corporation
        \\ * Copyright (c) 2015-2021 LunarG, Inc.
        \\ * Copyright (c) 2015-2021 Google Inc.
        \\ * Copyright (c) 2021 Advanced Micro Devices, Inc.All rights reserved.
        \\ *
        \\ ****************************************************************************/
        \\#pragma once
        \\
        \\#ifndef _INTRINSIC_EXTENSION_HEADER_H_
        \\#define _INTRINSIC_EXTENSION_HEADER_H_
    );
    try output_buffer.appendSlice("\n\n");

    var symbol_arena = std.heap.ArenaAllocator.init(allocator);
    defer symbol_arena.deinit();
    var symbol_names = std.ArrayList([]const u8).init(symbol_arena.allocator());
    defer symbol_names.deinit();
    var iter = input_dir.iterateAssumeFirstIteration();
    while (try iter.next()) |entry| {
        switch (entry.kind) {
            .file => if (std.mem.endsWith(u8, entry.name, ".glsl")) {
                var file = try input_dir.openFile(entry.name, .{});
                defer file.close();

                const symbol_name = try symbol_arena.allocator().dupe(u8, std.fs.path.stem(entry.name));
                try symbol_names.append(symbol_name);

                try output_buffer.appendSlice("std::string ");
                try output_buffer.appendSlice(symbol_name);
                try output_buffer.appendSlice("_GLSL = R\"(\n");
                try file.reader().readAllArrayList(&output_buffer, std.math.maxInt(usize));
                try output_buffer.appendSlice("\n)\";\n\n");
            },
            else => {},
        }
    }

    try output_buffer.appendSlice(
        "\nstd::string getIntrinsic(const char* const* shaders, int n) {\n" ++
        "\tstd::string shaderString = \"\";\n" ++
        "\tfor (int i = 0; i < n; i++) {\n"
    );

    for (symbol_names.items) |symbol_name| {
        try output_buffer.appendSlice("\t\tif (strstr(shaders[i], \"");
        try output_buffer.appendSlice(symbol_name);
        try output_buffer.appendSlice("\") != nullptr) {\n");
        try output_buffer.appendSlice("\t\t    shaderString.append(");
        try output_buffer.appendSlice(symbol_name);
        try output_buffer.appendSlice(
            "_GLSL);\n" ++
            "\t\t}\n"
        );
    }

    try output_buffer.appendSlice(
        "\t}\n" ++
        "\treturn shaderString;\n" ++
        "}\n" ++
        "\n" ++
        "#endif\n"
    );

    try write_buffer_to_file(cwd, output_file_path, output_buffer.items);

    return 0;
}
